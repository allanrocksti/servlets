package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
		name = "UsuariosPorId",
		urlPatterns = {"/get/param/usuarioPorId"}
		)
public class UsuariosPorId extends HttpServlet {
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	
    	String parametro = req.getParameter("id");
    	if(parametro == null)
    		 parametro = req.getAttribute("id").toString();
    	
        ServletOutputStream out = resp.getOutputStream();
        
        String saida = "";
        
        switch (parametro) {
		case "1":
			saida = "Allan Roque";
			break;
			
		case "2":
			saida = "Jackson Terceiro";
			break;
			
		case "3":
			saida = "Aluizio Neto";
			break;
			
		case "4":
			saida = "Daneiel Teixeira";
			break;

		default:
			saida = "Usuario nao encontrado";
			break;
		}
        
        out.write(saida.getBytes());
        out.flush();
        out.close();
    }

}
