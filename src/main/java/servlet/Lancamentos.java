package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
		name = "lancamentos",
		urlPatterns = {"/get/param/lancamentos"}
		)
public class Lancamentos extends HttpServlet {
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	
    	String parametro = req.getParameter("id");
    	if(parametro == null)
    		 parametro = req.getAttribute("id").toString();
    	
        ServletOutputStream out = resp.getOutputStream();
        
        String saida = "";
        
        switch (parametro) {
		case "1":
			saida = "Allan Roque\n\n"
					+ "01 - Camiseta meu partido e o brasil - 17,00 - 07/10/2018\n"
					+ "01 - Tinta verde - 0,50 - 07/10/2018\n"
					+ "01 - Tinta amarela - 0,50 - 07/10/2018\n"
					+ "01 - AR-15 - 8000,00 - 07/10/2018\n";
			break;
			
		case "2":
			saida = "Jackson Terceiro\n\n"
					+ "01 - Camiseta bolsonaro 2018 - 17,00 - 07/10/2018\n"
					+ "01 - Coxinha - 0,50 - 07/10/2018\n"
					+ "01 - Glock 9mm - 6000,00 - 07/10/2018\n";
			break;
			
		case "3":
			saida = "Aluizio Neto\n\n"
					+ "01 - Camiseta Lula livre - 13,00 - 07/10/2018\n"
					+ "01 - Pao com mortadela - cortesia - 07/10/2018\n"
					+ "01 - Foice - 13,00 - 07/10/2018\n"
					+ "01 - Martelo - 13,00 - 07/10/2018\n";
			break;
			
		case "4":
			saida = "Daniel Teixeira\n\n"
					+ "01 - Camiseta vamos questionar tudo - 17,00 - 07/10/2018\n"
					+ "01 - Esfirra cospida por alan dos santos - 30,00 - 07/10/2018\n"
					+ "01 - Perguntas - Gratis - 07/10/2018\n";
			break;

		default:
			saida = "Usuario nao encontrado";
			break;
		}
        
        out.write(saida.getBytes());
        out.flush();
        out.close();
    }

}