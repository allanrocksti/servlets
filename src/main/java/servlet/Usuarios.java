package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
		name = "Usuarios",
		urlPatterns = {"/get/usuarios"}
		)
public class Usuarios extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServletOutputStream out = resp.getOutputStream();
		
		String saida = "Allan Roque\nJackson Terceiro\nAluizio Neto\nDaneiel Teixeira";
		
        out.write(saida.getBytes());
        out.flush();
        out.close();
	}

	
}
